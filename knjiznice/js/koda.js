var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

//var baseUrlAPI = "http://apps.who.int/gho/athena/data/GHO/RS_196,RS_198.json?profile=simple&filter=COUNTRY:*";

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();
	
	$( "#sistolicni").empty();
	$( "#diastolicni").empty();
	document.getElementById("graf1").style.visibility = "hidden";
	document.getElementById("graf2").style.visibility = "hidden";
	
	var ehrId = $("#preberiEHRid").val();
	$("#StatistikaAPI").html(" ");
	
	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		zacetniIzpisStatistike(ehrId);
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
	    		console.log("data:   "+data);
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
          
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = "190";
	var telesnaTeza = "98";
	var telesnaTemperatura = "37";
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	var merilec = "Tonček";

	if (!ehrId || ehrId.trim().length == 0 || ehrId == "" || datumInUra == "" || sistolicniKrvniTlak == "" || diastolicniKrvniTlak == "" || nasicenostKrviSKisikom == "") {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
		var ehrId1 = document.getElementById('dodajMeritveVitalnihZnakovSporocilo').style.visibility = "visible";
	}
}

function generirajTriEhrje() {
	
	sessionId = getSessionId();
	
	var javniEhr1 = "";
var javniEhr2 = "";
var javniEhr3 = "";

	var ime1 = document.getElementById('kreirajIme')
	ime1.value = "Antonio";
	
	var priimek1 = document.getElementById('kreirajPriimek')
	priimek1.value = "Vivaldi";
	
	var datum1 = document.getElementById('kreirajDatumRojstva')
	datum1.value = "1741-07-28T08:44";
	
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        javniEhr1 = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                 /*   $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);*/
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
		
		/////////////////////////////////////////////////////////////////////////////////
			var ime2 = document.getElementById('kreirajIme')
	ime2.value = "Temüdžin 'Džingiskan'";
	
	var priimek2 = document.getElementById('kreirajPriimek')
	priimek2.value = "Bordžigin";
	
	var datum2 = document.getElementById('kreirajDatumRojstva')
	datum2.value = "1227-18-08T12:33";
	
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        javniEhr2 = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                   /* $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);*/
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
		
	/////////////////////////////////////////////////////////////////////////////////
	var ime3 = document.getElementById('kreirajIme')
	ime3.value = "Sungha";
	
	var priimek3 = document.getElementById('kreirajPriimek')
	priimek3.value = "Jung";
	
	var datum3 = document.getElementById('kreirajDatumRojstva')
	datum3.value = "1996-02-09T12:33";
	
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        javniEhr3 = data.ehrId;
				generirajPodatke(javniEhr1,javniEhr2,javniEhr3);
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                   /* $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);*/
		                    
		                    $( "#kreirajSporocilo").empty();	
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
}

/*var javniEhr1 ="094dc7ef-e7cd-4a31-b6a0-9ca1cfda0470";
var javniEhr2 ="c8bda4ac-00f3-4d26-9ad3-e759a09b1daf";
var javniEhr3 ="97dcbb9e-5309-41d1-91b9-1e7254c1195f";*/

/*var javniEhr1 = "26b2f06a-90f2-45fb-80a7-954c7dd9184e";
var javniEhr2 = "2abcc246-a706-43d9-97dd-db4964295d7a";
var javniEhr3 = "e17e7d31-26c8-4eae-b65b-740538c6667d";*/

function generirajPodatke(javniEhr1,javniEhr2,javniEhr3) {
	console.log("ena: " + javniEhr1);
	console.log("dve: " + javniEhr2);
	console.log("tri: " + javniEhr3);
	
	////////////////////////////// KREIRAM 1 KRVODAJALCA ////////////////////////////////
	var ime1 = document.getElementById('kreirajIme')
	ime1.value = "Uvuvwevwevwe Onyetenyevwe";
	
	var priimek1 = document.getElementById('kreirajPriimek')
	priimek1.value = "Ugwemubwem Ossas";
	
	var datum1 = document.getElementById('kreirajDatumRojstva')
	datum1.value = "1978-11-09T08:44";
	
	ime1.value = "";
	priimek1.value = "";
	datum1.value = "";
	
	var ehrId1 = document.getElementById('dodajVitalnoEHR').value = javniEhr1;
	var datumInUra1 = document.getElementById('dodajVitalnoDatumInUra').value = "1995-05-08T11:40Z";
	//var telesnaVisina1 = document.getElementById('dodajVitalnoTelesnaVisina').value = "180";
	//var telesnaTeza1 = document.getElementById('dodajVitalnoTelesnaTeza').value = "90";
	//var telesnaTemperatura1 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "37"
	var sistolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "95";
	var diastolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "115";
	var nasicenostKrviSKisikom1 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "89";
	//var merilec1 = document.getElementById('dodajVitalnoMerilec').value = "Opica Marta";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId1 = document.getElementById('dodajVitalnoEHR').value = javniEhr1;
	var datumInUra1 = document.getElementById('dodajVitalnoDatumInUra').value = "1996-05-08T11:40Z";
	//var telesnaVisina1 = document.getElementById('dodajVitalnoTelesnaVisina').value = "180";
	//var telesnaTeza1 = document.getElementById('dodajVitalnoTelesnaTeza').value = "90";
	//var telesnaTemperatura1 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "37"
	var sistolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "110";
	var diastolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "120";
	var nasicenostKrviSKisikom1 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "92";
	//var merilec1 = document.getElementById('dodajVitalnoMerilec').value = "Opica Marta";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId1 = document.getElementById('dodajVitalnoEHR').value = javniEhr1;
	var datumInUra1 = document.getElementById('dodajVitalnoDatumInUra').value = "1997-05-08T11:40Z";
	//var telesnaVisina1 = document.getElementById('dodajVitalnoTelesnaVisina').value = "180";
	//var telesnaTeza1 = document.getElementById('dodajVitalnoTelesnaTeza').value = "90";
	//var telesnaTemperatura1 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "37"
	var sistolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "115";
	var diastolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "125";
	var nasicenostKrviSKisikom1 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "94";
	//var merilec1 = document.getElementById('dodajVitalnoMerilec').value = "Opica Marta";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId1 = document.getElementById('dodajVitalnoEHR').value = javniEhr1;
	var datumInUra1 = document.getElementById('dodajVitalnoDatumInUra').value = "1998-05-08T11:40Z";
	//var telesnaVisina1 = document.getElementById('dodajVitalnoTelesnaVisina').value = "180";
	//var telesnaTeza1 = document.getElementById('dodajVitalnoTelesnaTeza').value = "90";
	//var telesnaTemperatura1 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "37"
	var sistolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "120";
	var diastolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "130";
	var nasicenostKrviSKisikom1 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "96";
	//var merilec1 = document.getElementById('dodajVitalnoMerilec').value = "Opica Marta";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId1 = document.getElementById('dodajVitalnoEHR').value = javniEhr1;
	var datumInUra1 = document.getElementById('dodajVitalnoDatumInUra').value = "1999-05-08T11:40Z";
	//var telesnaVisina1 = document.getElementById('dodajVitalnoTelesnaVisina').value = "180";
	//var telesnaTeza1 = document.getElementById('dodajVitalnoTelesnaTeza').value = "90";
	//var telesnaTemperatura1 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "37"
	var sistolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "125";
	var diastolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "135";
	var nasicenostKrviSKisikom1 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "98";
	//var merilec1 = document.getElementById('dodajVitalnoMerilec').value = "Opica Marta";
	
	dodajMeritveVitalnihZnakov();
	
	// pobirisi celice;
		var ehrId1 = document.getElementById('dodajVitalnoEHR').value = "";
	var datumInUra1 = document.getElementById('dodajVitalnoDatumInUra').value = "";
	//var telesnaVisina1 = document.getElementById('dodajVitalnoTelesnaVisina').value = "";
	//var telesnaTeza1 = document.getElementById('dodajVitalnoTelesnaTeza').value = "";
	//var telesnaTemperatura1 = document.getElementById('dodajVitalnoTelesnaTemperatura').value ="";
	var sistolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "";
	var diastolicniKrvniTlak1 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "";
	var nasicenostKrviSKisikom1 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "";
	//var merilec1 = document.getElementById('dodajVitalnoMerilec').value = "";

	////////////////////////////// KREIRAM 2 KRVODAJALCA ////////////////////////////////
	
	var ime2 = document.getElementById('kreirajIme')
	ime2.value = "Gozdni";
	
	var priimek2 = document.getElementById('kreirajPriimek')
	priimek2.value = "Joža";
	
	var datum2 = document.getElementById('kreirajDatumRojstva')
	datum2.value = "1995-11-09T12:33";
	
	ime2.value = "";
	priimek2.value = "";
	datum2.value = "";
	
	
	var ehrId2 = document.getElementById('dodajVitalnoEHR').value = javniEhr2;
	var datumInUra2 = document.getElementById('dodajVitalnoDatumInUra').value = "2000-05-08T11:40Z";
	//var telesnaVisina2 = document.getElementById('dodajVitalnoTelesnaVisina').value = "170";
	//var telesnaTeza2 = document.getElementById('dodajVitalnoTelesnaTeza').value = "120";
	//var telesnaTemperatura2 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "36"
	var sistolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "130";
	var diastolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "135";
	var nasicenostKrviSKisikom2 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "94";
	//var merilec2 = document.getElementById('dodajVitalnoMerilec').value = "Gospodič Toni";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId2 = document.getElementById('dodajVitalnoEHR').value = javniEhr2;
	var datumInUra2 = document.getElementById('dodajVitalnoDatumInUra').value = "2001-05-08T11:40Z";
	//var telesnaVisina2 = document.getElementById('dodajVitalnoTelesnaVisina').value = "170";
	//var telesnaTeza2 = document.getElementById('dodajVitalnoTelesnaTeza').value = "120";
	//var telesnaTemperatura2 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "36"
	var sistolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "132";
	var diastolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "134";
	var nasicenostKrviSKisikom2 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "94";
	//var merilec2 = document.getElementById('dodajVitalnoMerilec').value = "Gospodič Toni";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId2 = document.getElementById('dodajVitalnoEHR').value = javniEhr2;
	var datumInUra2 = document.getElementById('dodajVitalnoDatumInUra').value = "2002-05-08T11:40Z";
	//var telesnaVisina2 = document.getElementById('dodajVitalnoTelesnaVisina').value = "170";
	//var telesnaTeza2 = document.getElementById('dodajVitalnoTelesnaTeza').value = "120";
	//var telesnaTemperatura2 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "36"
	var sistolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "138";
	var diastolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "137";
	var nasicenostKrviSKisikom2 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "94";
	//var merilec2 = document.getElementById('dodajVitalnoMerilec').value = "Gospodič Toni";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId2 = document.getElementById('dodajVitalnoEHR').value = javniEhr2;
	var datumInUra2 = document.getElementById('dodajVitalnoDatumInUra').value = "2003-05-08T11:40Z";
	//var telesnaVisina2 = document.getElementById('dodajVitalnoTelesnaVisina').value = "170";
	//var telesnaTeza2 = document.getElementById('dodajVitalnoTelesnaTeza').value = "120";
	//var telesnaTemperatura2 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "36"
	var sistolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "142";
	var diastolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "150";
	var nasicenostKrviSKisikom2 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "94";
	//var merilec2 = document.getElementById('dodajVitalnoMerilec').value = "Gospodič Toni";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId2 = document.getElementById('dodajVitalnoEHR').value = javniEhr2;
	var datumInUra2 = document.getElementById('dodajVitalnoDatumInUra').value = "2004-05-08T11:40Z";
	//var telesnaVisina2 = document.getElementById('dodajVitalnoTelesnaVisina').value = "170";
	//var telesnaTeza2 = document.getElementById('dodajVitalnoTelesnaTeza').value = "120";
	//var telesnaTemperatura2 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "36"
	var sistolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "136";
	var diastolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "135";
	var nasicenostKrviSKisikom2 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "94";
	//var merilec2 = document.getElementById('dodajVitalnoMerilec').value = "Gospodič Toni";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId2 = document.getElementById('dodajVitalnoEHR').value = javniEhr2;
	var datumInUra2 = document.getElementById('dodajVitalnoDatumInUra').value = "2005-05-08T11:40Z";
	//var telesnaVisina2 = document.getElementById('dodajVitalnoTelesnaVisina').value = "170";
	//var telesnaTeza2 = document.getElementById('dodajVitalnoTelesnaTeza').value = "120";
	//var telesnaTemperatura2 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "36"
	var sistolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "148";
	var diastolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "142";
	var nasicenostKrviSKisikom2 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "94";
	//var merilec2 = document.getElementById('dodajVitalnoMerilec').value = "Gospodič Toni";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId2 = document.getElementById('dodajVitalnoEHR').value = javniEhr2;
	var datumInUra2 = document.getElementById('dodajVitalnoDatumInUra').value = "2006-05-08T11:40Z";
	//var telesnaVisina2 = document.getElementById('dodajVitalnoTelesnaVisina').value = "170";
	//var telesnaTeza2 = document.getElementById('dodajVitalnoTelesnaTeza').value = "120";
	//var telesnaTemperatura2 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "36"
	var sistolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "152";
	var diastolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "147";
	var nasicenostKrviSKisikom2 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "94";
	//var merilec2 = document.getElementById('dodajVitalnoMerilec').value = "Gospodič Toni";
	
	dodajMeritveVitalnihZnakov();
	
	var ehrId2 = document.getElementById('dodajVitalnoEHR').value = "";
	var datumInUra2 = document.getElementById('dodajVitalnoDatumInUra').value = "";
	//var telesnaVisina2 = document.getElementById('dodajVitalnoTelesnaVisina').value = "";
	//var telesnaTeza2 = document.getElementById('dodajVitalnoTelesnaTeza').value = "";
	//var telesnaTemperatura2 = document.getElementById('dodajVitalnoTelesnaTemperatura').value ="";
	var sistolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "";
	var diastolicniKrvniTlak2 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "";
	var nasicenostKrviSKisikom2 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "";
	//var merilec2 = document.getElementById('dodajVitalnoMerilec').value = "";
	
		////////////////////////////// KREIRAM 3 KRVODAJALCA ////////////////////////////////
	
	var ime3 = document.getElementById('kreirajIme')
	ime3.value = "Tabaluga";
	
	var priimek3 = document.getElementById('kreirajPriimek')
	priimek3.value = "Zmajček";
	
	var datum3 = document.getElementById('kreirajDatumRojstva')
	datum3.value = "2002-11-09T12:33";
	
	ime3.value = "";
	priimek3.value = "";
	datum3.value = "";
	
	var ehrId3 = document.getElementById('dodajVitalnoEHR').value = javniEhr3;
	var datumInUra3 = document.getElementById('dodajVitalnoDatumInUra').value = "2010-05-08T11:40Z";
	//var telesnaVisina3 = document.getElementById('dodajVitalnoTelesnaVisina').value = "95";
	//var telesnaTeza3 = document.getElementById('dodajVitalnoTelesnaTeza').value = "65";
	//var telesnaTemperatura3 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "1000"
	var sistolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "124";
	var diastolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "124";
	var nasicenostKrviSKisikom3 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "92";
	//var merilec3 = document.getElementById('dodajVitalnoMerilec').value = "Gospod Pisk";
	
	dodajMeritveVitalnihZnakov();
	
		
	var ehrId3 = document.getElementById('dodajVitalnoEHR').value = javniEhr3;
	var datumInUra3 = document.getElementById('dodajVitalnoDatumInUra').value = "2011-05-08T11:40Z";
	//var telesnaVisina3 = document.getElementById('dodajVitalnoTelesnaVisina').value = "95";
	//var telesnaTeza3 = document.getElementById('dodajVitalnoTelesnaTeza').value = "65";
	//var telesnaTemperatura3 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "1000"
	var sistolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "120";
	var diastolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "120";
	var nasicenostKrviSKisikom3 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "92";
	//var merilec3 = document.getElementById('dodajVitalnoMerilec').value = "Gospod Pisk";
	
	dodajMeritveVitalnihZnakov();
	
		
	var ehrId3 = document.getElementById('dodajVitalnoEHR').value = javniEhr3;
	var datumInUra3 = document.getElementById('dodajVitalnoDatumInUra').value = "2012-05-08T11:40Z";
	//var telesnaVisina3 = document.getElementById('dodajVitalnoTelesnaVisina').value = "95";
	//var telesnaTeza3 = document.getElementById('dodajVitalnoTelesnaTeza').value = "65";
	//var telesnaTemperatura3 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "1000"
	var sistolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "118";
	var diastolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "118";
	var nasicenostKrviSKisikom3 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "92";
	//var merilec3 = document.getElementById('dodajVitalnoMerilec').value = "Gospod Pisk";
	
	dodajMeritveVitalnihZnakov();
	
		
	var ehrId3 = document.getElementById('dodajVitalnoEHR').value = javniEhr3;
	var datumInUra3 = document.getElementById('dodajVitalnoDatumInUra').value = "2013-05-08T11:40Z";
	//var telesnaVisina3 = document.getElementById('dodajVitalnoTelesnaVisina').value = "95";
	//var telesnaTeza3 = document.getElementById('dodajVitalnoTelesnaTeza').value = "65";
	//var telesnaTemperatura3 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "1000"
	var sistolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "115";
	var diastolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "115";
	var nasicenostKrviSKisikom3 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "92";
	//var merilec3 = document.getElementById('dodajVitalnoMerilec').value = "Gospod Pisk";
	
	dodajMeritveVitalnihZnakov();
	
		
	var ehrId3 = document.getElementById('dodajVitalnoEHR').value = javniEhr3;
	var datumInUra3 = document.getElementById('dodajVitalnoDatumInUra').value = "2014-05-08T11:40Z";
	//var telesnaVisina3 = document.getElementById('dodajVitalnoTelesnaVisina').value = "95";
	//var telesnaTeza3 = document.getElementById('dodajVitalnoTelesnaTeza').value = "65";
	//var telesnaTemperatura3 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "1000"
	var sistolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "110";
	var diastolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "110";
	var nasicenostKrviSKisikom3 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "92";
	//var merilec3 = document.getElementById('dodajVitalnoMerilec').value = "Gospod Pisk";
	
	dodajMeritveVitalnihZnakov();
	
		
	var ehrId3 = document.getElementById('dodajVitalnoEHR').value = javniEhr3;
	var datumInUra3 = document.getElementById('dodajVitalnoDatumInUra').value = "2015-05-08T11:40Z";
	//var telesnaVisina3 = document.getElementById('dodajVitalnoTelesnaVisina').value = "95";
	//var telesnaTeza3 = document.getElementById('dodajVitalnoTelesnaTeza').value = "65";
	//var telesnaTemperatura3 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "1000"
	var sistolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "107";
	var diastolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "107";
	var nasicenostKrviSKisikom3 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "92";
	//var merilec3 = document.getElementById('dodajVitalnoMerilec').value = "Gospod Pisk";
	
	dodajMeritveVitalnihZnakov();
	
		
	var ehrId3 = document.getElementById('dodajVitalnoEHR').value = javniEhr3;
	var datumInUra3 = document.getElementById('dodajVitalnoDatumInUra').value = "2016-05-08T11:40Z";
	//var telesnaVisina3 = document.getElementById('dodajVitalnoTelesnaVisina').value = "95";
	//var telesnaTeza3 = document.getElementById('dodajVitalnoTelesnaTeza').value = "65";
	//var telesnaTemperatura3 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "1000"
	var sistolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "120";
	var diastolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "124";
	var nasicenostKrviSKisikom3 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "92";
	//var merilec3 = document.getElementById('dodajVitalnoMerilec').value = "Gospod Pisk";
	
	dodajMeritveVitalnihZnakov();
	
		
	var ehrId3 = document.getElementById('dodajVitalnoEHR').value = javniEhr3;
	var datumInUra3 = document.getElementById('dodajVitalnoDatumInUra').value = "2017-05-08T11:40Z";
	//var telesnaVisina3 = document.getElementById('dodajVitalnoTelesnaVisina').value = "95";
	//var telesnaTeza3 = document.getElementById('dodajVitalnoTelesnaTeza').value = "65";
	//var telesnaTemperatura3 = document.getElementById('dodajVitalnoTelesnaTemperatura').value = "1000"
	var sistolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "99";
	var diastolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "111";
	var nasicenostKrviSKisikom3 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "92";
	//var merilec3 = document.getElementById('dodajVitalnoMerilec').value = "Gospod Pisk";
	
	dodajMeritveVitalnihZnakov();
	
		var ehrId3 = document.getElementById('dodajVitalnoEHR').value = "";
	var datumInUra3 = document.getElementById('dodajVitalnoDatumInUra').value = "";
	//var telesnaVisina3 = document.getElementById('dodajVitalnoTelesnaVisina').value = "";
	//var telesnaTeza3 = document.getElementById('dodajVitalnoTelesnaTeza').value = "";
	//var telesnaTemperatura3 = document.getElementById('dodajVitalnoTelesnaTemperatura').value ="";
	var sistolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakSistolicni').value = "";
	var diastolicniKrvniTlak3 = document.getElementById('dodajVitalnoKrvniTlakDiastolicni').value = "";
	var nasicenostKrviSKisikom3 = document.getElementById('dodajVitalnoNasicenostKrviSKisikom').value = "";
	//var merilec3 = document.getElementById('dodajVitalnoMerilec').value = "";
	
	document.getElementById('dodajMeritveVitalnihZnakovSporocilo').style.visibility = "hidden";
	
	alert("Podatki so bili kreirani za slednje krvodajalce:\nAntonio Vivaldi: "+javniEhr1 + "\nTemüdžin 'Džingiskan' Bordžigin: " + javniEhr2 + "\nSungha Jung: "+ javniEhr3);
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
 var koda;
 var polje;
 var zurka;
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = "meritvah krvi";
	
	koda = ehrId;
	
	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
	    		zurka = data.party;
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov  <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.<br> Za vpogled podatkov kliknite na želen datum.</span><br/><br/>");

					
					
					
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
					    		polje = res;
					    		
						    	var results = "<table class='table table-striped " +
                    "table-hover' id='myTable'><tr><th>Datum in ura</th></tr>";
						        for (var i in res) {
						            results += "<tr><td class ='abc' onclick='NajdiElement(this)'>" + res[i].time + "</td>";
                          console.log(i);
						        }
						        results += "</table>";
						        
						        $("#rezultatMeritveVitalnihZnakov").append(results);
						        
						     /*  var results = "<table class='table table-striped " +
                    "table-hover' id='myTable'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Krvni Pritisk</th></tr>";
						        for (var i in res) {
						            results += "<tr><td class ='abc' onclick='NajdiElement(this)'>" + res[i].time +
                          "</td> <td class='text-right'>" + res[i].systolic +
                          " " + res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);*/
						        
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				
			},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

var tlakZaStatistiko;

function zacetniIzpisStatistike(ehrId) {
	//sessionId = getSessionId();
	
	var steviloDajatev = 0;
	
	/*	$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
	    		zurka = data.party;
				var party = data.party;*/
		
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	console.log("res: "+res);
					    	if(res == undefined){$("#StatistikaAPI").html("Do sedaj še niste darovali krvi!"); steviloDajatev = 0;}
					    	if (res.length > 0) {
					    		steviloDajatev = res.length;
					    		$("#StatistikaAPI").html("Ob zadnjem darovanju smo vam izmerili: <br> - Nivo <b>siastoličnega</b> tlaka, ki znaša: " + res[0].systolic + " mm Hg <br> - Nivo <b>diastoličnega</b> tlaka, ki znaša: " + res[0].diastolic + " mm Hg <br> V skladu z statistiko");
					    		tlakZaStatistiko = res[0].systolic;
					    			
								$( "#sistolicni").empty();	
								$( "#diastolicni").empty();
					    		document.getElementById("graf1").style.visibility = "visible";
					    		document.getElementById("graf2").style.visibility = "visible";
					    		loadDoc();
					    	}else {
						    	steviloDajatev = 0;
						    	$("#StatistikaAPI").html("Do sedaj še niste darovali krvi!");
							}
							preberiPodatkeZaVitalneZnake(ehrId);
						},
					    error: function() {
					    	$("#StatistikaAPI").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					
			/*},
	    	error: function(err) {
	    		$("#StatistikaAPI").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});*/
	
}

//var indexKlika;
function  NajdiElement(element) {
	console.log("element "+element);
    var indexKlika = element.parentNode.rowIndex;
	var yolo =element.parentNode.cellIndex;
  // console.log(indexKlika);
   var a = polje[indexKlika-1].systolic;
   //console.log("aaa");
    alert("Na ta dan so krvodajalcu " + zurka.firstNames + " " + zurka.lastNames + " izmerili: \n " + "Sistolični tlak: "+polje[indexKlika-1].systolic +"\n Diastolični tlak: " +polje[indexKlika-1].diastolic);
   // $('.abc').popover({title: "Sistolicni/diastolicni tlak v mm Hg + INDEX: " +indexKlika +" DRUGI: "+yolo, content: "Sistolični tlak: "+a +"\n Diastolični tlak: " +polje[indexKlika-1].diastolic}); 
	//bumBalonn();
}

function preberiPodatkeZaVitalneZnake(ehrId)
{
	//var sessionId = getSessionId();	
	$.ajaxSetup({
	    headers: {
	        "Ehr-Session": sessionId
	    }
	});

	var aql = "SELECT " +
    "a/content[openEHR-EHR-OBSERVATION.body_temperature.v1]/data[at0002]/events[at0003]/data[at0001]/items[at0004] as telTempVr, "+
    "a/content[openEHR-EHR-OBSERVATION.body_temperature.v1]/data[at0002]/events[at0003]/time as telTempCas, "+
    "a/content[openEHR-EHR-OBSERVATION.blood_pressure.v1]/data[at0001]/events[at0006]/data[at0003] as telKrvniTlak, "+
    "a/content[openEHR-EHR-OBSERVATION.blood_pressure.v1]/data[at0001]/events[at0006]/time as telKrvniTlakCas, " +
    "a/content[openEHR-EHR-OBSERVATION.height.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0004] as telVisinaVr, "+
    "a/content[openEHR-EHR-OBSERVATION.height.v1]/data[at0001]/events[at0002]/time as telVisinaCas, "+
    "a/content[openEHR-EHR-OBSERVATION.body_weight.v1]/data[at0002]/events[at0003]/data[at0001]/items[at0004] as telTezaVr, "+
    "a/content[openEHR-EHR-OBSERVATION.body_weight.v1]/data[at0002]/events[at0003]/time as telTezaCas, "+
    "a/content[openEHR-EHR-OBSERVATION.indirect_oximetry.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0006] as O2vKrvi, "+
    "a/content[openEHR-EHR-OBSERVATION.indirect_oximetry.v1]/data[at0001]/events[at0002]/time as casO2vKrvi, "+
    "a/territory/code_string as territory_code_string "+
	"from EHR e "+
	"contains COMPOSITION a[openEHR-EHR-COMPOSITION.encounter.v1] "+
	"where "+
	    "a/name/value='Vital Signs' and "+
	    "e/ehr_id/value='"+ ehrId +"' "+
	"offset 0 limit 100";	

	//console.log(aql);
	$.ajax({
	    url: baseUrl + "/query?" + $.param({"aql": aql}),
	    type: 'GET',
	    success: function (res) {
	        if(res != null)
	        {
	        	var rows = res.resultSet;
	        	//console.log(rows);	
		        naberiPodatkeKrvodajalca(rows);

		        // for (var i in rows) {
		        //     $("#result").append(rows[i].uid + ': ' + rows[i].name + ' (on ' +
		        //                         rows[i].time.value + ")<br>");
		        // }
	        } else
	        {
	        	console.log('AQL pozvedba za dan ehrId ni obrodila sadov - rezultat je prazna množica!');
	        }
	    },
	    error: function(err) {
			console.log(JSON.parse(err.responseText).userMessage);
		}
	});
}
function naberiPodatkeKrvodajalca(rows)
{
	console.log("rrrrrrrrrrow "+rows);
	var podatkiTelVisina = new Array();
	var podatkiTelTeza = new Array();
	var podatkiTelTemp = new Array();
	var podatkiTelTlak = new Array();
	var	podatkiO2vKrvi = new Array();

	var stevec = 0;
	
	for(var key in rows)
	{	
		var test = false;
		
		for(var b = 0; b<stevec;b++)
		{
			var zaPrimerjat = new Date(rows[key].telKrvniTlakCas.value);
			if(podatkiTelTlak[b].cas.getDate() == zaPrimerjat.getDate() && podatkiTelTlak[b].cas.getFullYear() == zaPrimerjat.getFullYear() && podatkiTelTlak[b].cas.getMonth() == zaPrimerjat.getMonth() && podatkiTelTlak[b].cas.getHours() == zaPrimerjat.getHours() && podatkiTelTlak[b].cas.getMinutes() == zaPrimerjat.getMinutes())
			{	
				test = true;
					podatkiTelTlak[b].sistolicni = rows[key].telKrvniTlak.items[0].value.magnitude;
					podatkiTelTlak[b].diastolicni = rows[key].telKrvniTlak.items[1].value.magnitude;
				
			}
		}
		
		if(test == true){}
		else{
			stevec++;
		
		// datum mertive
		var casMeritveVisine = rows[key].telVisinaCas.value;
		// visina v cm
		var vrednostMeritveVisine = rows[key].telVisinaVr.value.magnitude;
		
		var casMeritveTeze = rows[key].telTezaCas.value;
		var vrednostMeritveTeze = rows[key].telTezaVr.value.magnitude;

		var casMeritveTemp = rows[key].telTempCas.value;
		var vrednostMeritveTemp = rows[key].telTempVr.value.magnitude;

		var vrednostMeritveSisTlaka = rows[key].telKrvniTlak.items[0].value.magnitude; // systol
		var vrednostMeritveDiasTlaka = rows[key].telKrvniTlak.items[1].value.magnitude; // diastol
		var casMeritveKrvnegaTlaka = rows[key].telKrvniTlakCas.value;
		
		var casMeritveO2vKrvi = rows[key].casO2vKrvi.value;
		var vrednostMeritveO2vKrvi = rows[key].O2vKrvi.value.magnitude;

		podatkiTelVisina.push({"cas": new Date(casMeritveVisine), "visina":vrednostMeritveVisine});
		podatkiTelTeza.push({"cas": new Date(casMeritveTeze), "teza": vrednostMeritveTeze});
		podatkiTelTemp.push({"cas": new Date(casMeritveTemp), "temp": vrednostMeritveTemp});
		podatkiTelTlak.push({"cas": new Date(casMeritveKrvnegaTlaka), "sistolicni": vrednostMeritveSisTlaka, "diastolicni": vrednostMeritveDiasTlaka});
		podatkiO2vKrvi.push({"cas": new Date(casMeritveO2vKrvi), "O2vKrvi": vrednostMeritveO2vKrvi});
		}
	}

	// sortirana po datumu (najstarejši - najnovejši)
	podatkiTelVisina.sort(function(a,b){	
		if(a.cas < b.cas)
			return -1;
		else if(a.cas > b.cas)
			return 1;
		return 0;	
	});

	// sortirana po datumu (najstarejši - najnovejši)
	podatkiTelTemp.sort(function(a,b){	
		if(a.cas < b.cas)
			return -1;
		else if(a.cas > b.cas)
			return 1;
		return 0;	
	});

	podatkiTelTeza.sort(function(b,a){	
		if(a.cas < b.cas)
			return -1;
		else if(a.cas > b.cas)
			return 1;
		return 0;	
	});

	podatkiTelTlak.sort(function(b,a){	
		if(a.cas < b.cas)
			return -1;
		else if(a.cas > b.cas)
			return 1;
		return 0;	
	});
	
	podatkiO2vKrvi.sort(function(a,b){	
		if(a.cas < b.cas)
			return -1;
		else if(a.cas > b.cas)
			return 1;
		return 0;	
	});

	var zadnjaMeritevTeza = podatkiTelTeza[0];
	var zadnjaMeritevVisina = podatkiTelVisina[podatkiTelVisina.length-1];
	var zadnjaMeritevTlak  = podatkiTelTlak[0];
	var zadnjaMeritevTemperatura  = podatkiTelTemp[podatkiTelTemp.length-1];

	//obdelajObvestila({"teza":zadnjaMeritevTeza, "visina":zadnjaMeritevVisina, "tlak": zadnjaMeritevTlak, "temp":zadnjaMeritevTemperatura});

/*	telesnaVisinaIzpis(podatkiTelVisina);
	telesnaTezaIzpis(podatkiTelTeza);
	telesnaTemperaturaIzpis(podatkiTelTemp);
	krvniTlakIzpis(podatkiTelTlak);*/
	izpisSistolicni(podatkiTelTlak);
	izpisDiastolicni(podatkiTelTlak);
}

function izpisSistolicni(podatkiTelTlak)
{
	console.log("aaaaaaaaaaaaaa 0 "+podatkiTelTlak);

var margin = {
		top: 30,
		right: 20, 
		bottom: 86,
		left: 60
	},
		width = 500 - margin.left - margin.right,
		height = width - margin.top - margin.bottom;

	var x = d3.scale.ordinal()
		.rangeRoundBands([0, width], .06);

	var y = d3.scale.linear()
		.rangeRound([height, 0]);

	var color = d3.scale.ordinal()
		.range(["#308fef", "5fa9f3", "1176db"]);

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom")
		.tickFormat(d3.time.format("%Y-%m-%d"));

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left")
		.ticks(10);

	var svg = d3.select("#sistolicni").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");



	x.domain(podatkiTelTlak.map(function(d){
		return d.cas;
	}));	

	y.domain([0, d3.max(podatkiTelTlak, function(d){
		return d.sistolicni;
	})]);

	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + height + ")")
		.call(xAxis)
	.selectAll("text")
		.style("text-anchor", "end")
		.attr("dx", "0.60em")
		.attr("dy", "0.80em")
		.attr("transform", "rotate(-45)" )
	.append("text")
		.attr("dy", "-.82em")
      	.style("text-anchor", "end");

	svg.append("g")
		.attr("class", "y axis")
		.call(yAxis)
	.append("text")
      	.attr("dy", "-.82em")
      	.style("text-anchor", "end")
      	.text(" (mm Hg)");

    var sistolicni_info = $("#sistolicni_info");

    svg.selectAll("bar")
    	.data(podatkiTelTlak)
    .enter().append("rect")
    	.style("fill", "steelblue")
    	.style("opacity", .8)
    	.attr("x", function(d) { return x(d.cas); })
    	.attr("width", x.rangeBand())
    	.attr("y", function(d) {return y(d.sistolicni); })
    	.attr("height", function(d) { return height - y(d.sistolicni); })

    	.on("mouseover", function(d){
    		sistolicni_info.css("visibility", "visible");
    		sistolicni_info.html("<small style='color:green; font-style:italic;'>" + d.cas + " </small>&nbsp;&nbsp;&nbsp;&nbsp;" + d.sistolicni + "<small> mm Hg</small>");
    		$(this).css("fill", "green");

    	})
    	.on("mouseout", function(d){
    		sistolicni_info.css("visibility", "hidden");
    		$(this).css("fill", "steelblue");
    	});

}

function izpisDiastolicni(podatkiTelTlak)
{

var margin = {
		top: 30,
		right: 20, 
		bottom: 86,
		left: 60
	},
		width = 500 - margin.left - margin.right,
		height = width - margin.top - margin.bottom;

	var x = d3.scale.ordinal()
		.rangeRoundBands([0, width], .06);

	var y = d3.scale.linear()
		.rangeRound([height, 0]);

	var color = d3.scale.ordinal()
		.range(["#308fef", "5fa9f3", "1176db"]);

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom")
		.tickFormat(d3.time.format("%Y-%m-%d"));

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left")
		.ticks(10);

	var svg = d3.select("#diastolicni").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");



	x.domain(podatkiTelTlak.map(function(d){
		return d.cas;
	}));	

	y.domain([0, d3.max(podatkiTelTlak, function(d){
		return d.diastolicni;
	})]);

	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + height + ")")
		.call(xAxis)
	.selectAll("text")
		.style("text-anchor", "end")
		.attr("dx", "0.60em")
		.attr("dy", "0.80em")
		.attr("transform", "rotate(-45)" )
	.append("text")
		.attr("dy", "-.82em")
      	.style("text-anchor", "end");

	svg.append("g")
		.attr("class", "y axis")
		.call(yAxis)
	.append("text")
      	.attr("dy", "-.82em")
      	.style("text-anchor", "end")
      	.text(" (mm Hg)");

    var diastolicni_info = $("#diastolicni_info");

    svg.selectAll("bar")
    	.data(podatkiTelTlak)
    .enter().append("rect")
    	.style("fill", "steelblue")
    	.style("opacity", .8)
    	.attr("x", function(d) { return x(d.cas); })
    	.attr("width", x.rangeBand())
    	.attr("y", function(d) {return y(d.diastolicni); })
    	.attr("height", function(d) { return height - y(d.diastolicni); })

    	.on("mouseover", function(d){
    		diastolicni_info.css("visibility", "visible");
    		diastolicni_info.html("<small style='color:green; font-style:italic;'>" + d.cas + " </small>&nbsp;&nbsp;&nbsp;&nbsp;" + d.diastolicni + "<small> mm Hg</small>");
    		$(this).css("fill", "green");

    	})
    	.on("mouseout", function(d){
    		diastolicni_info.css("visibility", "hidden");
    		$(this).css("fill", "steelblue");
    	});

}


// Zunanji API
function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    myFunction(this);
    }
  };
  xhttp.open("GET", "knjiznice/XML_data/data.xml", true);
  xhttp.send();
}

function myFunction(xml) {
	console.log("sfdsfsdfsd "+ xml);
  var i;
  var xmlDoc = xml.responseXML;
  var table="<tr><th>Region</th><th>Display</th></tr>";
  var x = xmlDoc.getElementsByTagName("Fact");
  //console.log(x);
  /*for (i = 0; i <x.length; i++) { 
    table += "<tr><td>" +
    x[i].getElementsByTagName("REGION")[0].childNodes[0].nodeValue +
    "</td><td>" +
    x[i].getElementsByTagName("Display")[0].childNodes[0].nodeValue +
    "</td></tr>";
    
    console.log("aa");
  }
  document.getElementById("demo").innerHTML = table;*/
  
//  $("#StatistikaAPI").html("");
	var male = x[0].getElementsByTagName("Display")[0].childNodes[0].nodeValue;
	var maleSplit = male.split(" ");
	
	var female = x[1].getElementsByTagName("Display")[0].childNodes[0].nodeValue;
	var femaleSplit = female.split(" ");
	
	var sestevek = parseFloat(maleSplit[0])+parseFloat(femaleSplit[0]);
	var povprecje = (sestevek)/2;
	console.log(povprecje);
	
  document.getElementById("StatistikaAPI").innerHTML += " iz leta " + x[0].getElementsByTagName("YEAR")[0].childNodes[0].nodeValue +", povprečni sistolični tlak znaša " + povprecje + " mm Hg";
  
  if((povprecje-4) <= tlakZaStatistiko && (povprecje+4) >= tlakZaStatistiko)
  {
  	document.getElementById("StatistikaAPI").innerHTML += "<br> <span id='GreenRed'>Vaš krvni tlak je popoln. Zdravi ste kot dren!<span>";
	document.getElementById("GreenRed").style.color = "green";
  }else
   if(povprecje > tlakZaStatistiko)
  {
  	document.getElementById("StatistikaAPI").innerHTML += "<br> <span id='GreenRed'>Ker je vaš tlak veliko manjši od povprečja vam svetujemo da čimprej obiščete osebnega zdravnika</span>";
  	document.getElementById("GreenRed").style.color = "red";
  }
  else
  {
	document.getElementById("StatistikaAPI").innerHTML += "<br> <span id='GreenRed'> Ker je vaš tlak veliko večji od povprečja vam svetujemo da čimprej obiščete osebnega zdravnika </span>";
  	document.getElementById("GreenRed").style.color = "red";
  }
 // if(povprecje<)
}


$(document).ready(function() {
	// ČUDA
    /*var circle = new ProgressBar.Circle('#kanta', {
        color: '#FCB03C',
        duration: 3000,
        easing: 'easeInOut'
    });

    circle.animate(1);*/
//
	/*console.log("aaa");
    	$.ajax({
            url: baseUrlAPI,
            type: "GET",
            success: function (data) {
            	console.log(data.dimension);
            }
        });
    */
    
   // document.getElementById("graf1").style.visibility = "hidden";
    
   /*var ehrId;
   ehrId = "82408854-055f-4bce-96ad-b8e55e0ddc24";
   preberiPodatkeZaVitalneZnake(ehrId);*/
   
   //document.getElementById("nevidno").style.visibility = "collapse";
   
  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
   
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		//$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		//$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		//$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		//$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});